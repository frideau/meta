;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defsystem :meta
  :version "1.0.0"
  :description "META syntax to easily write parsers"
  :long-description "META syntax to easily write parsers, as per Henry G. Baker's Prag-Parse article"
  :author "Jochen Schmidt"
  :depends-on ("named-readtables")
  :components ((:file "package")
	       (:file "meta-src" :depends-on ("package"))))
